import org.testng.annotations.Test;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import org.hamcrest.*;
import org.hamcrest.core.IsEqual;

public class PostRequests {
	
	String baseURI = "https://jsonplaceholder.typicode.com";
	
	@Test
	public void CreateNewPost(){
		String apiBody="{\"title\" :\"Manik\", \"body\":\"Hello\", \"userId\":10}";
		
		System.out.println("sending request to server-->"+baseURI+"/posts");
		
		RequestSpecBuilder builder=new RequestSpecBuilder();
		builder.setBody(apiBody);
		builder.setContentType("application/json");
		RequestSpecification specification=builder.build();
		Response response=given().when().spec(specification).post(baseURI+"/posts");
		
		String body=response.getBody().asString();
		int statusCode=response.getStatusCode();
		
		System.out.println(body);
		assertEquals(statusCode, 201);
		
	}

}
