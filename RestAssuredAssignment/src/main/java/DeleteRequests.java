import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;


public class DeleteRequests {
	
	String baseURI="https://jsonplaceholder.typicode.com";
	
	@Test
	public void deletePost(){
	int postId=10;
		
	Response response=given().pathParam("postId", postId).
	when().delete(baseURI+"/posts/{postId}");
	assertEquals(response.statusCode(), 200);
	
	List<Integer> allPosts=given().get(baseURI+"/posts").jsonPath().getList("id");
	System.out.println("Total Number of Posts-->"+allPosts.size()+""
			+"\n Post Deleted whose id--> "+postId);
	for(int id:allPosts){
		if(id==postId){
			assertTrue(false);
		}
			
	}
		
	}

}
