

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;


public class PutRequests {
	String baseURI="https://jsonplaceholder.typicode.com";
	int newUserId=17;
	
	@Test
	public void updatePost(){
		int existingPostId=10;
		int newUserId=17;
		String Updatedbody="{\"userId\":"+newUserId+",\"id\": 10,\"title\": \"optio molestias id quia eum\",\"body\":\"quo et expedita modi "
				+ "cum officia vel magnindoloribus qui repudiandaenvero nisi sitnquos veniam quod "
				+ "sed accusamus veritatis error\"}";
		
		System.out.println("sending request to server-->"+baseURI+"/posts/"+existingPostId);
		
		String oldPost=given().when().get(baseURI+"/posts/"+existingPostId).asString();
		System.out.println(oldPost+
				"\n<-----------Updated Post-------------->");
		
		RequestSpecBuilder builder=new RequestSpecBuilder();
		builder.setContentType("application/json");
		builder.setBody(Updatedbody);
		RequestSpecification spec=builder.build();
		
		Response response=given().pathParam("existingPostId", existingPostId).spec(spec).
		when().put(baseURI+"/posts/{existingPostId}");
		
		System.out.println(response.asString());
		
		assertEquals(response.body().jsonPath().getInt("userId"), newUserId);
		
		
	}

}
