import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.*;

import org.hamcrest.core.IsEqual;

public class GetRequests {
	String baseURI = "https://jsonplaceholder.typicode.com";

	@Test
	public void getAllUsers() {
		System.out.println("sending request to server--> " + baseURI + "/users");

		String allUserslist = given().
				              when().get(baseURI + "/users").
				              then().assertThat().
				              statusCode(200).and()
				              .contentType(ContentType.JSON).extract().asString();
		
		System.out.println(allUserslist);
	}

	@Test
	public void getUserById() {
		int userId = 10;
		System.out.println("sending request to server--> " + baseURI + "/users/" + userId);
		String user = given().pathParam("userId", userId).
				      when().get(baseURI + "/users/{userId}").
				      then().assertThat()
				      .statusCode(200).and().
				      body("id", IsEqual.equalTo(userId)).extract().asString();
		
		System.out.println(user);
	}

	@Test
	public void getAllPosts() {
		System.out.println("sending request to server-->" + baseURI + "/posts");
		ValidatableResponse response = given().when().get(baseURI + "/posts").then();
        response.assertThat().statusCode(200);
        
		String posts=response.extract().asString();
		System.out.println(posts);

	}

}
